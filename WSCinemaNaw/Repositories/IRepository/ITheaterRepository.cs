﻿using WSCinemaNaw.Models;

namespace WSCinemaNaw.Repositories.IRepository
{
    public interface ITheaterRepository
    {
        Task<IEnumerable<Theater>> GetAllAsync();
        Task<Theater> GetByIdAsync(int id);
        Task AddAsync(Theater theater);
        Task UpdateAsync(Theater theater);
        Task DeleteAsync(int id);
    }
}
