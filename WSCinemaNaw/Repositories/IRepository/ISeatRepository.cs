﻿using WSCinemaNaw.Models;

namespace WSCinemaNaw.Repositories.IRepository
{
    public interface ISeatRepository
    {
        Task<IEnumerable<Seat>> GetAllAsync();
        Task<Seat> GetByIdAsync(int id);
        Task AddAsync(Seat seat);
        Task UpdateAsync(Seat seat);
        Task DeleteAsync(int id);
        Task<IEnumerable<Seat>> GetAllWithTheatersAsync();
        Task<List<Seat>> GetSeatsByRoomIdAsync(int roomId);
    }
}
