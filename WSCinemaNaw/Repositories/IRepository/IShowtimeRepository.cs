﻿using WSCinemaNaw.Models;

namespace WSCinemaNaw.Repositories.IRepository
{
    public interface IShowtimeRepository
    {
        Task<IEnumerable<Showtime>> GetAllAsync();
        Task<Showtime> GetByIdAsync(int id);
        Task AddAsync(Showtime showtime);
        Task UpdateAsync(Showtime showtime);
        Task DeleteAsync(int id);
        Task<IEnumerable<Showtime>> GetAllWithTheatersAsync();
        Task<IEnumerable<Showtime>> GetByRoomIdAsync(int roomId);
    }
}
