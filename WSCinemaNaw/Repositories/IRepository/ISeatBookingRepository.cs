﻿
using WSCinemaNaw.Models;

namespace WSCinemaNaw.Repositories.IRepository
{
    public interface ISeatBookingRepository
    {
        Task<IEnumerable<SeatBooking>> GetAllAsync();
        Task<SeatBooking> GetByIdAsync(int id);
        Task AddAsync(SeatBooking seatBooking);
        Task UpdateAsync(SeatBooking seatBooking);
        Task DeleteAsync(int id);
    }
}

