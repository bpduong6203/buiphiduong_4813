﻿using WSCinemaNaw.Models;

namespace WSCinemaNaw.Repositories.IRepository
{
    public interface IDirectorRepository
    {
        Task<IEnumerable<Director>> GetAllAsync();
        Task<Director> GetByIdAsync(int id);
        Task AddAsync(Director director);
        Task UpdateAsync(Director director);
        Task DeleteAsync(int id);
    }
}
