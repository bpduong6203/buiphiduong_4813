﻿using Microsoft.EntityFrameworkCore;
using WSCinemaNaw.Data;
using WSCinemaNaw.Models;
using WSCinemaNaw.Repositories.IRepository;

namespace WSCinemaNaw.Repositories.EFRepository
{
    public class EFShowtimeRepository : IShowtimeRepository
    {
        private readonly ApplicationDbContext _context;
        public EFShowtimeRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Showtime>> GetAllAsync()
        {
            return await _context.Showtimes.ToListAsync();
        }
        public async Task<Showtime> GetByIdAsync(int id)
        {
            return await _context.Showtimes.FindAsync(id);
        }
        public async Task AddAsync(Showtime showtime)
        {
            _context.Showtimes.Add(showtime);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateAsync(Showtime showtime)
        {
            _context.Showtimes.Update(showtime);
            await _context.SaveChangesAsync();
        }
        public async Task DeleteAsync(int id)
        {
            var showtime = await _context.Showtimes.FindAsync(id);
            _context.Showtimes.Remove(showtime);
            await _context.SaveChangesAsync();
        }
        public async Task<IEnumerable<Showtime>> GetAllWithTheatersAsync()
        {
            return await _context.Showtimes
                .Include(r => r.Movie)
                .Include(r => r.Room)
                .Include(r => r.Theater)
                .ToListAsync();
        }
        public async Task<IEnumerable<Showtime>> GetByRoomIdAsync(int roomId)
        {
            return await _context.Showtimes.Where(s => s.RoomID == roomId).ToListAsync();
        }
    }
}
