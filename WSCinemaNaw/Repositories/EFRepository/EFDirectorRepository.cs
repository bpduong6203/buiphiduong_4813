﻿using Microsoft.EntityFrameworkCore;
using WSCinemaNaw.Data;
using WSCinemaNaw.Models;
using WSCinemaNaw.Repositories.IRepository;

namespace WSCinemaNaw.Repositories.EFRepository
{
    public class EFDirectorRepository : IDirectorRepository
    {
        private readonly ApplicationDbContext _context;
        public EFDirectorRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Director>> GetAllAsync()
        {
            return await _context.Directors.ToListAsync();
        }
        public async Task<Director> GetByIdAsync(int id)
        {
            return await _context.Directors.FindAsync(id);
        }
        public async Task AddAsync(Director director)
        {
            _context.Directors.Add(director);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateAsync(Director director)
        {
            _context.Directors.Update(director);
            await _context.SaveChangesAsync();
        }
        public async Task DeleteAsync(int id)
        {
            var director = await _context.Directors.FindAsync(id);
            _context.Directors.Remove(director);
            await _context.SaveChangesAsync();
        }
    }
}
