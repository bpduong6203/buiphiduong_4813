﻿
using Microsoft.EntityFrameworkCore;
using WSCinemaNaw.Data;
using WSCinemaNaw.Models;
using WSCinemaNaw.Repositories.IRepository;

namespace WSCinemaNaw.Repositories.EFRepository
{
    public class EFSeatBookingRepository : ISeatBookingRepository
    {
        private readonly ApplicationDbContext _context;
        public EFSeatBookingRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<SeatBooking>> GetAllAsync()
        {
            return await _context.SeatBookings.ToListAsync();
        }
        public async Task<SeatBooking> GetByIdAsync(int id)
        {
            return await _context.SeatBookings.FindAsync(id);
        }
        public async Task AddAsync(SeatBooking seatBooking)
        {
            _context.SeatBookings.Add(seatBooking);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateAsync(SeatBooking seatBooking)
        {
            _context.SeatBookings.Update(seatBooking);
            await _context.SaveChangesAsync();
        }
        public async Task DeleteAsync(int id)
        {
            var seatBooking = await _context.SeatBookings.FindAsync(id);
            _context.SeatBookings.Remove(seatBooking);
            await _context.SaveChangesAsync();
        }
    }
}
