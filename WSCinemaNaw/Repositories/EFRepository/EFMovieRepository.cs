﻿using Microsoft.EntityFrameworkCore;
using WSCinemaNaw.Data;
using WSCinemaNaw.Models;
using WSCinemaNaw.Repository.IRepository;


namespace WSCinemaNaw.Repository.EFRepository
{
    public class EFMovieRepository : IMovieRepository
    {
        private readonly ApplicationDbContext _context;
        public EFMovieRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Movie>> GetAllAsync()
        {
            return await _context.Movies.ToListAsync();
        }
        public async Task<Movie> GetByIdAsync(int id)
        {
            return await _context.Movies.FindAsync(id);
        }
        public async Task AddAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateAsync(Movie movie)
        {
            _context.Movies.Update(movie);
            await _context.SaveChangesAsync();
        }
        public async Task DeleteAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }
        public async Task<IEnumerable<Movie>> GetAllWithTheatersAsync()
        {
            return await _context.Movies
                .Include(r => r.Genre)
                .Include(r => r.Director)
                .Include(r => r.Country)
                .ToListAsync();
        }
    }
}
