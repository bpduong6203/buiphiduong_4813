﻿using Microsoft.EntityFrameworkCore;
using WSCinemaNaw.Data;
using WSCinemaNaw.Models;
using WSCinemaNaw.Repositories.IRepository;

namespace WSCinemaNaw.Repositories.EFRepository
{
    public class EFTheaterRepository : ITheaterRepository
    {
        private readonly ApplicationDbContext _context;
        public EFTheaterRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Theater>> GetAllAsync()
        {
            return await _context.Theaters.ToListAsync();
        }
        public async Task<Theater> GetByIdAsync(int id)
        {
            return await _context.Theaters.FindAsync(id);
        }
        public async Task AddAsync(Theater theater)
        {
            _context.Theaters.Add(theater);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateAsync(Theater theater)
        {
            _context.Theaters.Update(theater);
            await _context.SaveChangesAsync();
        }
        public async Task DeleteAsync(int id)
        {
            var theater = await _context.Theaters.FindAsync(id);
            _context.Theaters.Remove(theater);
            await _context.SaveChangesAsync();
        }
    }
}
