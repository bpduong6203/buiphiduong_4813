﻿using Microsoft.EntityFrameworkCore;
using WSCinemaNaw.Data;
using WSCinemaNaw.Models;
using WSCinemaNaw.Repositories.IRepository;

namespace WSCinemaNaw.Repositories.EFRepository
{
    public class EFSeatRepository : ISeatRepository
    {
        private readonly ApplicationDbContext _context;
        public EFSeatRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Seat>> GetAllAsync()
        {
            return await _context.Seats.ToListAsync();
        }
        public async Task<Seat> GetByIdAsync(int id)
        {
            return await _context.Seats.FindAsync(id);
        }
        public async Task AddAsync(Seat seat)
        {
            _context.Seats.Add(seat);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateAsync(Seat seat)
        {
            _context.Seats.Update(seat);
            await _context.SaveChangesAsync();
        }
        public async Task DeleteAsync(int id)
        {
            var seat = await _context.Seats.FindAsync(id);
            _context.Seats.Remove(seat);
            await _context.SaveChangesAsync();
        }
        public async Task<IEnumerable<Seat>> GetAllWithTheatersAsync()
        {
            return await _context.Seats
                .Include(r => r.Room)
                .ToListAsync();
        }
        public async Task<List<Seat>> GetSeatsByRoomIdAsync(int roomId)
        {
            return await _context.Seats.Where(s => s.RoomID == roomId).ToListAsync();
        }
    }
}
