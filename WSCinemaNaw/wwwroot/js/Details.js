﻿$(document).ready(function () {
    var movieId = $('#movieId').val();

    $.get('/Movies/GetDatesByMovieId', { movieId: movieId }, function (data) {
        var dateButtons = '';
        for (var i = 0; i < data.length; i++) {
            var date = new Date(data[i]);
            var formattedDate = date.toLocaleDateString('en-CA');
            dateButtons += '<button class="date-btn" value="' + formattedDate + '">' + formattedDate + '</button>';
        }
        $('#date-buttons-container').html(dateButtons);
    });

    $(document).on('click', '.date-btn', function () {
        $('.date-btn').removeClass('selected');
        $(this).addClass('selected');

        var date = $(this).val();
        var movieId = $('#movieId').val();

        $.post('/Movies/GetTimesByDate', { date: date, movieId: movieId }, function (data) {
            var timeButtons = '';
            for (var i = 0; i < data.length; i++) {
                var time = new Date('1970-01-01T' + data[i] + 'Z');
                var hours = time.getUTCHours().toString().padStart(2, '0');
                var minutes = time.getUTCMinutes().toString().padStart(2, '0');
                var formattedTime = hours + ':' + minutes;
                timeButtons += '<button class="time-btn" value="' + formattedTime + '">' + formattedTime + '</button>';
            }
            $('#time-buttons-container').html(timeButtons);
        }).fail(function () {
            alert('Có lỗi xảy ra khi tải thời gian chiếu.');
        });
    });


    $(document).on('click', '.time-btn', function () {
        var time = $(this).val();
        var date = $('.date-btn.selected').val();
        var movieId = $('#movieId').val();

        $.post('/Movies/GetSeatTime', { date: date, time: time, movieId: movieId }, function (data) {
            var seatButtons = '';
            for (var i = 0; i < data.length; i++) {
                var seat = data[i];

                if (!seat.isBooked) {
                    var buttonClass = seat.isBooked ? 'booked-seat' : (seat.isAvailable ? 'available-seat' : 'unavailable-seat');
                    var seatLabel = '  ' + seat.rowNumber + '  ' + seat.seatNumber;
                    seatButtons += '<button class="seat-btn ' + buttonClass + '" value="' + seat.SeatID + '">' + seatLabel + '</button>';
                }
            }
            $('#seat-buttons-container').html(seatButtons);
        });

    });

    $(document).on('click', '.seat-btn', function () {
        var movieId = $('#movieId').val();
        $.post('/Order/Checkout', { movieId: movieId }, function (data) {
            window.location.href = '/Order/Create';
        });
    });
});


function playTrailer() {
    var video = document.getElementById('trailer-video');
    var poster = document.querySelector('.movie-poster');
    var pauseButton = document.getElementById('pause-button');
    var exitButton = document.getElementById('exit-button');
    var main = document.getElementById('main');
    var details = document.getElementById('movie-details');
    video.style.display = 'block';
    pauseButton.style.display = 'block';
    exitButton.style.display = 'block';
    details.style.display = 'none';
    poster.style.display = 'none';
    main.style.justifyContent = 'center';
    video.play();
}

function pauseVideo() {
    var video = document.getElementById('trailer-video');
    if (video.paused) {
        video.play();
    } else {
        video.pause();
    }
}

function exitVideo() {
    var video = document.getElementById('trailer-video');
    var poster = document.querySelector('.movie-poster');
    var pauseButton = document.getElementById('pause-button');
    var exitButton = document.getElementById('exit-button');
    var details = document.getElementById('movie-details');
    video.style.display = 'none';
    pauseButton.style.display = 'none';
    exitButton.style.display = 'none';
    details.style.display = 'block';
    video.pause();
    video.style.display = 'none';
    poster.style.display = 'block';
}