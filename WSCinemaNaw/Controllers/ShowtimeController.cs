﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Globalization;
using WSCinemaNaw.Data;
using WSCinemaNaw.Models;
using WSCinemaNaw.Repositories.IRepository;
using WSCinemaNaw.Repository.IRepository;

namespace WSCinemaNaw.Controllers
{
    public class ShowtimeController : Controller
    {
        private readonly IShowtimeRepository _showtimeRepository;
        private readonly IMovieRepository _movieRepository;
        private readonly ITheaterRepository _theaterRepository;
        private readonly IRoomRepository _roomRepository;
        private readonly ApplicationDbContext _context;

        public ShowtimeController(IShowtimeRepository showtimeRepository, IMovieRepository movieRepository, ITheaterRepository heaterRepository, IRoomRepository roomRepository, ApplicationDbContext context)
        {
            _showtimeRepository = showtimeRepository;
            _theaterRepository = heaterRepository;
            _roomRepository = roomRepository;
            _movieRepository = movieRepository;
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var showtime = await _showtimeRepository.GetAllWithTheatersAsync();
            return View(showtime);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var showtime = await _showtimeRepository.GetByIdAsync(id.Value);
            if (showtime == null)
            {
                return NotFound();
            }

            ViewBag.Movies = new SelectList(await _movieRepository.GetAllAsync(), "MovieID", "Title");
            ViewBag.Rooms = new SelectList(await _roomRepository.GetAllAsync(), "RoomID", "Name");
            ViewBag.Theaters = new SelectList(await _theaterRepository.GetAllAsync(), "TheaterID", "Name");
            return View(showtime);
        }

        public async Task<IActionResult> Create()
        {

            ViewBag.Movies = new SelectList(await _movieRepository.GetAllAsync(), "MovieID", "Title");
            ViewBag.Rooms = new SelectList(await _roomRepository.GetAllAsync(), "RoomID", "Name");
            ViewBag.Theaters = new SelectList(await _theaterRepository.GetAllAsync(), "TheaterID", "Name");
            return View();
        }

        private async Task SaveShowtime(Showtime showtime, string releaseDate)
        {
            showtime.ShowtimeDate = ConvertToDateTime(releaseDate);
            await _showtimeRepository.AddAsync(showtime);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Showtime showtime, string releaseDate)
        {
            if (ModelState.IsValid)
            {
                await SaveShowtime(showtime, releaseDate);
                return RedirectToAction(nameof(Index));
            }

            ViewBag.Movies = new SelectList(await _movieRepository.GetAllAsync(), "MovieID", "Title", showtime.MovieID);
            ViewBag.Rooms = new SelectList(await _roomRepository.GetAllAsync(), "RoomID", "Name", showtime.RoomID);
            ViewBag.Theaters = new SelectList(await _theaterRepository.GetAllAsync(), "TheaterID", "Name", showtime.TheaterID);
            return View(showtime);
        }


        public DateTime ConvertToDateTime(string releaseDate)
        {
            string format = "yyyy-MM-ddTHH:mm";
            CultureInfo provider = new CultureInfo("vi-VN");
            DateTime dateTime = DateTime.ParseExact(releaseDate, format, provider);
            return dateTime;
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var showtime = await _showtimeRepository.GetByIdAsync(id.Value);
            if (showtime == null)
            {
                return NotFound();
            }
            ViewBag.Movies = new SelectList(await _movieRepository.GetAllAsync(), "MovieID", "Title", showtime.MovieID);
            ViewBag.Rooms = new SelectList(await _roomRepository.GetAllAsync(), "RoomID", "Name", showtime.RoomID);
            ViewBag.Theaters = new SelectList(await _theaterRepository.GetAllAsync(), "TheaterID", "Name", showtime.TheaterID);
            return View(showtime);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, Showtime showtime, string releaseDate)
        {
            if (id != showtime.ShowtimeID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var existingMovie = await _showtimeRepository.GetByIdAsync(id);
                string format = "yyyy-MM-ddTHH:mm";
                CultureInfo provider = new CultureInfo("vi-VN");

                await _showtimeRepository.UpdateAsync(showtime);
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Movies = new SelectList(await _movieRepository.GetAllAsync(), "MovieID", "Title", showtime.MovieID);
            ViewBag.Rooms = new SelectList(await _roomRepository.GetAllAsync(), "RoomID", "Name", showtime.RoomID);
            ViewBag.Theaters = new SelectList(await _theaterRepository.GetAllAsync(), "TheaterID", "Name", showtime.TheaterID);
            return View(showtime);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var showtime = await _showtimeRepository.GetByIdAsync(id.Value);
            if (showtime == null)
            {
                return NotFound();
            }
            ViewBag.Movies = new SelectList(await _movieRepository.GetAllAsync(), "MovieID", "Title", showtime.MovieID);
            ViewBag.Rooms = new SelectList(await _roomRepository.GetAllAsync(), "RoomID", "Name", showtime.RoomID);
            ViewBag.Theaters = new SelectList(await _theaterRepository.GetAllAsync(), "TheaterID", "Name", showtime.TheaterID);
            return View(showtime);
        }

        [HttpPost, ActionName("DeleteConfirmed")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _showtimeRepository.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
