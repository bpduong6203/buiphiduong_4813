﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WSCinemaNaw.Data;
using WSCinemaNaw.Models;
using WSCinemaNaw.Repositories.IRepository;

namespace WSCinemaNaw.Controllers
{
    public class SeatBookingController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly ISeatRepository _seatRepository;
        private readonly IShowtimeRepository _showtimeRepository;


        public SeatBookingController(ApplicationDbContext context, IShowtimeRepository showtimeRepository, ISeatRepository seatRepository)
        {
            _context = context;
            _showtimeRepository = showtimeRepository;
            _seatRepository = seatRepository;
        }

        // GET: SeatBooking
        public async Task<IActionResult> Index()
        {
            ViewBag.Showtimes = new SelectList(await _showtimeRepository.GetAllAsync(), "ShowtimeID", "ShowtimeDate");
            return View(await _context.SeatBookings.ToListAsync());
        }

        // GET: SeatBooking/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var seatBooking = await _context.SeatBookings
                .FirstOrDefaultAsync(m => m.SeatBookingID == id);
            if (seatBooking == null)
            {
                return NotFound();
            }

            return View(seatBooking);
        }

        // GET: SeatBooking/Create
        // GET: SeatBooking/Create
        public async Task<IActionResult> Create()
        {
            ViewBag.Seats = new SelectList(await _seatRepository.GetAllAsync(), "SeatID", "SeatID");
            ViewBag.Showtimes = new SelectList(await _showtimeRepository.GetAllAsync(), "ShowtimeID", "ShowtimeDate");
            return View();
        }


        // POST: SeatBooking/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SeatBookingID,SeatID,ShowtimeID,IsBooked,IsAvailable")] SeatBooking seatBooking)
        {
            if (ModelState.IsValid)
            {
                _context.Add(seatBooking);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Seats = new SelectList(await _seatRepository.GetAllAsync(), "SeatD", "SeatID", seatBooking.SeatID);
            ViewBag.Showtimes = new SelectList(await _showtimeRepository.GetAllAsync(), "ShowtimeID", "ShowtimeDate", seatBooking.ShowtimeID);
            return View(seatBooking);
        }

        // GET: SeatBooking/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var seatBooking = await _context.SeatBookings.FindAsync(id);
            if (seatBooking == null)
            {
                return NotFound();
            }
            return View(seatBooking);
        }

        // POST: SeatBooking/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SeatBookingID,SeatID,ShowtimeID,IsBooked,IsAvailable")] SeatBooking seatBooking)
        {
            if (id != seatBooking.SeatBookingID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(seatBooking);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SeatBookingExists(seatBooking.SeatBookingID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(seatBooking);
        }

        // GET: SeatBooking/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var seatBooking = await _context.SeatBookings
                .FirstOrDefaultAsync(m => m.SeatBookingID == id);
            if (seatBooking == null)
            {
                return NotFound();
            }

            return View(seatBooking);
        }

        // POST: SeatBooking/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var seatBooking = await _context.SeatBookings.FindAsync(id);
            _context.SeatBookings.Remove(seatBooking);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SeatBookingExists(int id)
        {
            return _context.SeatBookings.Any(e => e.SeatBookingID == id);
        }
    }
}
