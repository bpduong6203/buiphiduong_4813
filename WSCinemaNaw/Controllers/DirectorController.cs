﻿using Microsoft.AspNetCore.Mvc;
using WSCinemaNaw.Models;
using WSCinemaNaw.Repositories.IRepository;

namespace WSCinemaNaw.Controllers
{
    public class DirectorController : Controller
    {
        private readonly IDirectorRepository _directorRepository;

        public DirectorController(IDirectorRepository directorRepository)
        {
            _directorRepository = directorRepository;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _directorRepository.GetAllAsync());
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var director = await _directorRepository.GetByIdAsync(id.Value);
            if (director == null)
            {
                return NotFound();
            }

            return View(director);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Director director)
        {
            if (ModelState.IsValid)
            {
                await _directorRepository.AddAsync(director);
                return RedirectToAction(nameof(Index));
            }
            return View(director);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var director = await _directorRepository.GetByIdAsync(id.Value);
            if (director == null)
            {
                return NotFound();
            }
            return View(director);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Director director)
        {
            if (id != director.DirectorID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                await _directorRepository.UpdateAsync(director);
                return RedirectToAction(nameof(Index));
            }
            return View(director);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var director = await _directorRepository.GetByIdAsync(id);
            if (director == null)
            {
                return NotFound();
            }
            return View(director);
        }

        [HttpPost, ActionName("DeleteConfirmed")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _directorRepository.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
