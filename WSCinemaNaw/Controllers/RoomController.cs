﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WSCinemaNaw.Models;
using WSCinemaNaw.Repositories.IRepository;

namespace WSCinemaNaw.Controllers
{
    public class RoomController : Controller
    {
        private readonly IRoomRepository _roomRepository;
        private readonly ITheaterRepository _theaterRepository;

        public RoomController(IRoomRepository roomRepository, ITheaterRepository theaterRepository)
        {
            _roomRepository = roomRepository;
            _theaterRepository = theaterRepository;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _roomRepository.GetAllWithTheatersAsync());
        }


        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var room = await _roomRepository.GetByIdAsync(id.Value);
            if (room == null)
            {
                return NotFound();
            }
            ViewBag.Theaters = new SelectList(await _theaterRepository.GetAllAsync(), "TheaterID", "Name");
            return View(room);
        }

        public async Task<IActionResult> Create()
        {
            ViewBag.Theaters = new SelectList(await _theaterRepository.GetAllAsync(), "TheaterID", "Name");
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Room room)
        {
            if (ModelState.IsValid)
            {
                await _roomRepository.AddAsync(room);
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Theaters = new SelectList(await _theaterRepository.GetAllAsync(), "TheaterID", "Name", room.TheaterID);
            return View(room);
        }



        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var room = await _roomRepository.GetByIdAsync(id.Value);
            if (room == null)
            {
                return NotFound();
            }
            ViewBag.Theaters = new SelectList(await _theaterRepository.GetAllAsync(), "TheaterID", "Name", room.TheaterID);
            return View(room);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Room room)
        {
            if (id != room.RoomID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                await _roomRepository.UpdateAsync(room);
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Theaters = new SelectList(await _theaterRepository.GetAllAsync(), "TheaterID", "Name", room.TheaterID);
            return View(room);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var room = await _roomRepository.GetByIdAsync(id);
            if (room == null)
            {
                return NotFound();
            }
            return View(room);
        }

        [HttpPost, ActionName("DeleteConfirmed")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _roomRepository.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
