﻿using Microsoft.AspNetCore.Mvc;
using WSCinemaNaw.Models;
using WSCinemaNaw.Repositories.IRepository;

namespace WSCinemaNaw.Areas.Admin.Controllers
{
    public class TheaterController : Controller
    {
        private readonly ITheaterRepository _theaterRepository;

        public TheaterController(ITheaterRepository theaterRepository)
        {
            _theaterRepository = theaterRepository;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _theaterRepository.GetAllAsync());
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var theater = await _theaterRepository.GetByIdAsync(id.Value);
            if (theater == null)
            {
                return NotFound();
            }

            return View(theater);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Theater theater)
        {
            if (ModelState.IsValid)
            {
                await _theaterRepository.AddAsync(theater);
                return RedirectToAction(nameof(Index));
            }
            return View(theater);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var theater = await _theaterRepository.GetByIdAsync(id.Value);
            if (theater == null)
            {
                return NotFound();
            }
            return View(theater);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Theater theater)
        {
            if (id != theater.TheaterID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                await _theaterRepository.UpdateAsync(theater);
                return RedirectToAction(nameof(Index));
            }
            return View(theater);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var theater = await _theaterRepository.GetByIdAsync(id.Value);
            if (theater == null)
            {
                return NotFound();
            }

            return View(theater);
        }

        [HttpPost, ActionName("DeleteConfirmed")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _theaterRepository.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
