﻿using Microsoft.AspNetCore.Mvc;
using WSCinemaNaw.Models;
using WSCinemaNaw.Repositories.IRepository;

namespace WSCinemaNaw.Controllers
{
    public class RatingController : Controller
    {
        private readonly IRatingRepository _ratingRepository;

        public RatingController(IRatingRepository ratingRepository)
        {
            _ratingRepository = ratingRepository;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _ratingRepository.GetAllAsync());
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rating = await _ratingRepository.GetByIdAsync(id.Value);
            if (rating == null)
            {
                return NotFound();
            }

            return View(rating);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Rating rating)
        {
            if (ModelState.IsValid)
            {
                await _ratingRepository.AddAsync(rating);
                return RedirectToAction(nameof(Index));
            }
            return View(rating);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rating = await _ratingRepository.GetByIdAsync(id.Value);
            if (rating == null)
            {
                return NotFound();
            }
            return View(rating);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Rating rating)
        {
            if (id != rating.RatingID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                await _ratingRepository.UpdateAsync(rating);
                return RedirectToAction(nameof(Index));
            }
            return View(rating);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rating = await _ratingRepository.GetByIdAsync(id.Value);
            if (rating == null)
            {
                return NotFound();
            }

            return View(rating);
        }

        [HttpPost, ActionName("DeleteConfirmed")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _ratingRepository.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
