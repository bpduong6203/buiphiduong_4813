﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using WSCinemaNaw.Data;
using WSCinemaNaw.Models;
using WSCinemaNaw.Repositories.IRepository;
using WSCinemaNaw.Repository.IRepository;

namespace WSCinemaNaw.Areas.Admin.Controllers
{
    public class MoviesController : Controller
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IDirectorRepository _directorRepository;
        private readonly ICountryRepository _countryRepository;
        private readonly IGenreRepository _genreRepository;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IShowtimeRepository _showtimeRepository;
        private readonly ISeatRepository _seatRepository;
        private readonly ApplicationDbContext _context;

        public MoviesController(IMovieRepository movieRepository, ICountryRepository countryRepository, IGenreRepository genreRepository,
            IDirectorRepository directorRepository, IWebHostEnvironment hostingEnvironment, ApplicationDbContext context,
            IShowtimeRepository showtimeRepository, ISeatRepository seatRepository)

        {
            _movieRepository = movieRepository;
            _countryRepository = countryRepository;
            _genreRepository = genreRepository;
            _directorRepository = directorRepository;
            _hostingEnvironment = hostingEnvironment;
            _context = context;
            _showtimeRepository = showtimeRepository;
            _seatRepository = seatRepository;
        }

        public async Task<IActionResult> Index()
        {
            var movie = await _movieRepository.GetAllWithTheatersAsync();
            return View(movie);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movie = await _movieRepository.GetByIdAsync(id.Value);
            if (movie == null)
            {
                return NotFound();
            }

            // Lấy danh sách các ngày có sẵn từ cơ sở dữ liệu
            var dates = _context.Showtimes.Where(s => s.MovieID == id).Select(s => s.ShowtimeDate.Date).Distinct().ToList();

            // Tạo một ViewModel mới
            var viewModel = new MovieDetailsViewModel
            {
                Movie = movie,
                Dates = dates
            };

            ViewBag.Countries = new SelectList(await _countryRepository.GetAllAsync(), "CountryID", "Name");
            ViewBag.Directors = new SelectList(await _directorRepository.GetAllAsync(), "DirectorID", "Name");
            ViewBag.Genres = new SelectList(await _genreRepository.GetAllAsync(), "GenreID", "Name");

            // Trả về ViewModel để hiển thị trên giao diện người dùng
            return View(viewModel);
        }

        public async Task<IActionResult> Create()
        {
            ViewBag.Countries = new SelectList(await _countryRepository.GetAllAsync(), "CountryID", "Name");
            ViewBag.Directors = new SelectList(await _directorRepository.GetAllAsync(), "DirectorID", "Name");
            ViewBag.Genres = new SelectList(await _genreRepository.GetAllAsync(), "GenreID", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Movie movie, IFormFile posterImage, IFormFile trailerVideo, string releaseDate)
        {
            if (ModelState.IsValid)
            {
                if (posterImage != null)
                {
                    movie.PosterImage = await SaveImage(posterImage);
                }
                if (trailerVideo != null)
                {
                    movie.TrailerVideo = await SaveImage(trailerVideo);
                }
                string format = "yyyy-MM-ddTHH:mm";
                CultureInfo provider = new CultureInfo("vi-VN");
                movie.ReleaseDate = DateTime.ParseExact(releaseDate, format, provider);

                await _movieRepository.AddAsync(movie);
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Countries = new SelectList(await _countryRepository.GetAllAsync(), "CountryID", "Name", movie.CountryID);
            ViewBag.Directors = new SelectList(await _directorRepository.GetAllAsync(), "DirectorID", "Name", movie.DirectorID);
            ViewBag.Genres = new SelectList(await _genreRepository.GetAllAsync(), "GenreID", "Name", movie.GenreID);
            ViewBag.Showtime = new SelectList(await _showtimeRepository.GetAllAsync(), "ShowtimeID", "Showtimedate");
            ViewBag.Seat = new SelectList(await _seatRepository.GetAllAsync(), "SeatID", "SeatID");
            return View(movie);
        }

        private async Task<string> SaveImage(IFormFile image)
        {
            var savePath = Path.Combine("wwwroot/Image", image.FileName);
            using (var fileStream = new FileStream(savePath, FileMode.Create))
            {
                await image.CopyToAsync(fileStream);
            }
            return "/Image/" + image.FileName;
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movie = await _movieRepository.GetByIdAsync(id.Value);
            if (movie == null)
            {
                return NotFound();
            }
            ViewBag.Countries = new SelectList(await _countryRepository.GetAllAsync(), "CountryID", "Name", movie.CountryID);
            ViewBag.Directors = new SelectList(await _directorRepository.GetAllAsync(), "DirectorID", "Name", movie.DirectorID);
            ViewBag.Genres = new SelectList(await _genreRepository.GetAllAsync(), "GenreID", "Name", movie.GenreID);
            return View(movie);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, Movie movie, IFormFile newPosterImage, IFormFile newTrailerVideo, string releaseDate)
        {
            if (id != movie.MovieID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var existingMovie = await _movieRepository.GetByIdAsync(id);

                if (newPosterImage != null)
                {
                    existingMovie.PosterImage = await SaveImage(newPosterImage);
                }
                if (newTrailerVideo != null)
                {
                    existingMovie.TrailerVideo = await SaveImage(newTrailerVideo);
                }
                string format = "yyyy-MM-ddTHH:mm";
                CultureInfo provider = new CultureInfo("vi-VN");
                DateTime parsedReleaseDate;
                if (DateTime.TryParseExact(releaseDate, format, provider, DateTimeStyles.None, out parsedReleaseDate))
                {
                    existingMovie.ReleaseDate = parsedReleaseDate;
                }
                existingMovie.CountryID = movie.CountryID;
                existingMovie.DirectorID = movie.DirectorID;
                existingMovie.GenreID = movie.GenreID;

                await _movieRepository.UpdateAsync(existingMovie);
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Countries = new SelectList(await _countryRepository.GetAllAsync(), "CountryID", "Name", movie.CountryID);
            ViewBag.Directors = new SelectList(await _directorRepository.GetAllAsync(), "DirectorID", "Name", movie.DirectorID);
            ViewBag.Genres = new SelectList(await _genreRepository.GetAllAsync(), "GenreID", "Name", movie.GenreID);
            return View(movie);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movie = await _movieRepository.GetByIdAsync(id.Value);
            if (movie == null)
            {
                return NotFound();
            }
            ViewBag.Countries = new SelectList(await _countryRepository.GetAllAsync(), "CountryID", "Name");
            ViewBag.Directors = new SelectList(await _directorRepository.GetAllAsync(), "DirectorID", "Name");
            ViewBag.Genres = new SelectList(await _genreRepository.GetAllAsync(), "GenreID", "Name");
            return View(movie);
        }

        [HttpPost, ActionName("DeleteConfirmed")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _movieRepository.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }


        [HttpPost]
        public ActionResult GetSeatsByDate(DateTime date, TimeSpan time, int movieId)
        {
            var movie = _context.Movies.Find(movieId);
            var showtime = _context.Showtimes.FirstOrDefault(s => s.ShowtimeDate.Date == date.Date && s.ShowtimeDate.TimeOfDay == time && s.MovieID == movieId);

            var seatBookings = new List<SeatBooking>();
            if (showtime != null)
            {
                seatBookings = _context.SeatBookings.Where(sb => sb.ShowtimeID == showtime.ShowtimeID).ToList();
            }

            var viewModel = new MovieDetailsViewModel
            {
                Movie = movie,
                SeatBookings = seatBookings,
                Dates = _context.Showtimes.Where(s => s.MovieID == movieId).Select(s => s.ShowtimeDate.Date).Distinct().ToList(),
                Times = _context.Showtimes.Where(s => s.MovieID == movieId).Select(s => s.ShowtimeDate.TimeOfDay).Distinct().ToList()
            };

            return View(viewModel);
        }


        [HttpPost]
        public ActionResult GetTimesByDate(DateTime date, int movieId)
        {
            var times = _context.Showtimes
                .Where(s => s.MovieID == movieId && s.ShowtimeDate.Date == date.Date)
                .Select(s => s.ShowtimeDate.TimeOfDay)
                .Distinct()
                .ToList();

            return Json(times);
        }

        [HttpPost]
        public async Task<IActionResult> GetSeatTime(DateTime date, TimeSpan time, int movieId)
        {
            var showtime = await _context.Showtimes.FirstOrDefaultAsync(s => s.ShowtimeDate.Date == date.Date && s.ShowtimeDate.TimeOfDay == time && s.MovieID == movieId);

            var seats = new List<SeatBooking>();
            if (showtime != null)
            {
                seats = await _context.SeatBookings
                    .Include(sb => sb.Seat)
                    .Where(sb => sb.ShowtimeID == showtime.ShowtimeID)
                    .ToListAsync();
            }

            return Json(seats.Select(s => new { s.SeatBookingID, s.SeatID, s.IsBooked, s.IsAvailable, s.Seat.RowNumber, s.Seat.SeatNumber }));
        }



        [HttpGet]
        public ActionResult GetDatesByMovieId(int movieId)
        {
            var dates = _context.Showtimes
                .Where(s => s.MovieID == movieId)
                .Select(s => s.ShowtimeDate.Date)
                .Distinct()
                .ToList();

            return Json(dates);
        }
    }
}
