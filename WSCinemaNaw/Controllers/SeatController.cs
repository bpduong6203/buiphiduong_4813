﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WSCinemaNaw.Data;
using WSCinemaNaw.Models;
using WSCinemaNaw.Repositories.IRepository;

namespace WSCinemaNaw.Controllers
{
    public class SeatController : Controller
    {
        private readonly ISeatRepository _seatRepository;
        private readonly IRoomRepository _roomRepository;
        private readonly ApplicationDbContext _context;

        public SeatController(ISeatRepository seatRepository, IRoomRepository roomRepository, ApplicationDbContext applicationDbContext)
        {
            _seatRepository = seatRepository;
            _roomRepository = roomRepository;
            _context = applicationDbContext;
        }

        public async Task<IActionResult> Index()
        {
            var seat = await _seatRepository.GetAllWithTheatersAsync();
            return View(seat);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var seat = await _seatRepository.GetByIdAsync(id.Value);
            if (seat == null)
            {
                return NotFound();
            }
            return View(seat);
        }

        public async Task<IActionResult> Create()
        {
            ViewBag.Rooms = new SelectList(await _roomRepository.GetAllAsync(), "RoomID", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Seat seat)
        {
            if (ModelState.IsValid)
            {
                await _seatRepository.AddAsync(seat);
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Rooms = new SelectList(await _roomRepository.GetAllAsync(), "RoomID", "Name", seat.RoomID);
            return View(seat);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var seat = await _seatRepository.GetByIdAsync(id.Value);
            if (seat == null)
            {
                return NotFound();
            }
            ViewBag.Rooms = new SelectList(await _roomRepository.GetAllAsync(), "RoomID", "Name", seat.RoomID);
            return View(seat);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Seat seat)
        {
            if (id != seat.SeatID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                await _seatRepository.UpdateAsync(seat);
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Rooms = new SelectList(await _roomRepository.GetAllAsync(), "RoomID", "Name", seat.RoomID);
            return View(seat);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var seat = await _seatRepository.GetByIdAsync(id.Value);
            if (seat == null)
            {
                return NotFound();
            }

            return View(seat);
        }

        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> Delete(int id)
        {
            await _seatRepository.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
