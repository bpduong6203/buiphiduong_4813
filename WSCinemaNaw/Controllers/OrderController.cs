﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WSCinemaNaw.Data;
using WSCinemaNaw.Models;
using WSCinemaNaw.Repositories.IRepository;
using WSCinemaNaw.Repository.IRepository;

namespace WSCinemaNaw.Controllers
{
    public class OrderController : Controller
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderDetailRepository _orderDetailRepository;
        private readonly ITicketRepository _ticketRepository;
        private readonly ISeatBookingRepository _seatBookingRepository;
        private readonly ApplicationDbContext _context;

        public OrderController(IOrderRepository orderRepository, IMovieRepository movieRepository, IOrderDetailRepository orderDetailRepository
            , ITicketRepository ticketRepository, ISeatBookingRepository seatBookingRepository, ApplicationDbContext context)
        {
            _orderRepository = orderRepository;
            _movieRepository = movieRepository;
            _orderDetailRepository = orderDetailRepository;
            _ticketRepository = ticketRepository;
            _seatBookingRepository = seatBookingRepository;
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _orderRepository.GetAllAsync());
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _orderRepository.GetByIdAsync(id.Value);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        public IActionResult Create(int? movieId, int? seatBookingId)
        {
            if (movieId.HasValue)
            {
                ViewBag.MovieId = movieId.Value;
            }
            else
            {
                ViewBag.MovieId = HttpContext.Session.GetInt32("movieId");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Order order)
        {
            if (ModelState.IsValid)
            {
                await _orderRepository.AddAsync(order);
                return Json(new { success = true, orderId = order.OrderID });
            }
            return Json(new { success = false, error = "Invalid order data" });
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _orderRepository.GetByIdAsync(id.Value);
            if (order == null)
            {
                return NotFound();
            }
            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Order order)
        {
            if (id != order.OrderID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                await _orderRepository.UpdateAsync(order);
                return RedirectToAction(nameof(Index));
            }
            return View(order);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _orderRepository.GetByIdAsync(id.Value);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        [HttpPost, ActionName("DeleteConfirmed")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _orderRepository.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public ActionResult Checkout(int movieId)
        {
            var showtime = _context.Showtimes.FirstOrDefault(s => s.MovieID == movieId);
            if (showtime == null)
            {
                // Xử lý trường hợp không tìm thấy showtime
                // ...
            }
            HttpContext.Session.SetInt32("movieId", movieId);
            var seatBookings = _context.SeatBookings.Where(sb => sb.ShowtimeID == showtime.ShowtimeID).ToList();
            return Json(seatBookings.Select(sb => sb.SeatBookingID));
        }

        [HttpPost]
        public ActionResult GetSeatsByDate(DateTime date, TimeSpan time, int movieId)
        {
            var movie = _context.Movies.Find(movieId);
            var showtime = _context.Showtimes.FirstOrDefault(s => s.ShowtimeDate.Date == date.Date && s.ShowtimeDate.TimeOfDay == time && s.MovieID == movieId);

            var seatBookings = new List<SeatBooking>();
            if (showtime != null)
            {
                seatBookings = _context.SeatBookings.Where(sb => sb.ShowtimeID == showtime.ShowtimeID).ToList();
            }

            var viewModel = new MovieDetailsViewModel
            {
                Movie = movie,
                SeatBookings = seatBookings,
                Dates = _context.Showtimes.Where(s => s.MovieID == movieId).Select(s => s.ShowtimeDate.Date).Distinct().ToList(),
                Times = _context.Showtimes.Where(s => s.MovieID == movieId).Select(s => s.ShowtimeDate.TimeOfDay).Distinct().ToList()
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult GetTimesByDate(DateTime date, int movieId)
        {
            var times = _context.Showtimes
                .Where(s => s.MovieID == movieId && s.ShowtimeDate.Date == date.Date)
                .Select(s => s.ShowtimeDate.TimeOfDay)
                .Distinct()
                .ToList();

            return Json(times);
        }

        [HttpPost]
        public async Task<IActionResult> GetSeatTime(DateTime date, TimeSpan time, int movieId)
        {
            var showtime = await _context.Showtimes.FirstOrDefaultAsync(s => s.ShowtimeDate.Date == date.Date && s.ShowtimeDate.TimeOfDay == time && s.MovieID == movieId);

            var seats = new List<SeatBooking>();
            if (showtime != null)
            {
                seats = await _context.SeatBookings
                    .Include(sb => sb.Seat)
                    .Where(sb => sb.ShowtimeID == showtime.ShowtimeID)
                    .ToListAsync();
            }

            return Json(seats.Select(s => new { s.SeatBookingID, s.SeatID, s.IsBooked, s.IsAvailable, s.Seat.RowNumber, s.Seat.SeatNumber }));
        }

        [HttpGet]
        public ActionResult GetDatesByMovieId(int movieId)
        {
            var dates = _context.Showtimes
                .Where(s => s.MovieID == movieId)
                .Select(s => s.ShowtimeDate.Date)
                .Distinct()
                .ToList();

            return Json(dates);
        }

        //đặt ghế 
        [HttpPost]
        public async Task<IActionResult> ConfirmSeats(string date, string time, int movieId, int[] seatBookingID)
        {
            var selectedSeats = await _context.SeatBookings
                .Where(sb => seatBookingID.Contains(sb.SeatBookingID))
                .ToListAsync();

            foreach (var seat in selectedSeats)
            {
                seat.IsReserved = true;
            }

            await _context.SaveChangesAsync();

            var tickets = await _context.Tickets
                .Where(t => selectedSeats.Select(s => s.SeatBookingID).Contains(t.SeatBookingID))
                .ToListAsync();

            var order = new Order
            {
                OrderDate = DateTime.Now,
                TotalAmount = 0
            };

            _context.Orders.Add(order);
            await _context.SaveChangesAsync();
            int orderID = order.OrderID;

            foreach (var ticket in tickets)
            {
                var orderDetail = new OrderDetail
                {
                    OrderID = order.OrderID,
                    TicketID = ticket.TicketID,
                    Price = ticket.Price
                };

                _context.OrderDetails.Add(orderDetail);
            }

            decimal totalAmount = tickets.Sum(t => t.Price);

            order.TotalAmount = totalAmount;

            await _context.SaveChangesAsync();

            return Json(new { success = true, orderId = orderID, totalAmount = totalAmount });
        }

        //Thanh toán 
        [HttpPost]
        public async Task<IActionResult> MakePayment(int orderId, string paymentMethod)
        {
            var order = await _context.Orders.FindAsync(orderId);

            if (order == null)
            {
                return NotFound();
            }

            var payment = new Payment
            {
                OrderID = orderId,
                Amount = order.TotalAmount,
                PaymentDate = DateTime.Now,
                PaymentMethod = paymentMethod
            };

            _context.Payments.Add(payment);

            var selectedSeats = await _context.SeatBookings
                .Where(sb => _context.OrderDetails.Any(od => od.OrderID == orderId && od.Ticket.SeatBookingID == sb.SeatBookingID))
                .ToListAsync();

            foreach (var seat in selectedSeats)
            {
                seat.IsBooked = true;
                seat.IsReserved = false;
            }

            await _context.SaveChangesAsync();

            return Json(new { success = true });
        }
    }
}

