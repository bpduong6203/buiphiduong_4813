﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WSCinemaNaw.Models;
using WSCinemaNaw.Repositories.IRepository;

namespace WSCinemaNaw.Controllers
{
    public class TicketController : Controller
    {
        private readonly ITicketRepository _ticketRepository;
        private readonly ISeatBookingRepository _seatBookingRepository;

        public TicketController(ITicketRepository ticketRepository, ISeatBookingRepository seatBookingRepository)
        {
            _ticketRepository = ticketRepository;
            _seatBookingRepository = seatBookingRepository;
        }

        public async Task<IActionResult> Index()
        {
            var tickets = (await _ticketRepository.GetAllAsync())
               .OrderBy(t => t.SeatBookingID)
               .ToList();
            return View(tickets);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = await _ticketRepository.GetByIdAsync(id.Value);
            if (ticket == null)
            {
                return NotFound();
            }

            return View(ticket);
        }

        public async Task<IActionResult> Create()
        {
            var usedSeatBookingIDs = (await _ticketRepository.GetAllAsync())
                .Select(t => t.SeatBookingID)
                .ToList();
            var allSeatBookings = await _seatBookingRepository.GetAllAsync();
            var availableSeatBookings = allSeatBookings
                .Where(sb => !usedSeatBookingIDs.Contains(sb.SeatBookingID))
                .ToList();
            ViewBag.SeatBookings = new SelectList(availableSeatBookings, "SeatBookingID", "SeatBookingID");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Ticket ticket, List<int> SeatBookingIDs)
        {
            if (ModelState.IsValid)
            {
                foreach (var seatBookingID in SeatBookingIDs)
                {
                    var newTicket = new Ticket
                    {
                        SeatBookingID = seatBookingID,
                        Price = ticket.Price
                    };
                    await _ticketRepository.AddAsync(newTicket);
                }
                return RedirectToAction(nameof(Index));
            }
            var usedSeatBookingIDs = (await _ticketRepository.GetAllAsync())
                .Select(t => t.SeatBookingID)
                .ToList();
            var allSeatBookings = await _seatBookingRepository.GetAllAsync();
            var availableSeatBookings = allSeatBookings
                .Where(sb => !usedSeatBookingIDs.Contains(sb.SeatBookingID))
                .ToList();
            ViewBag.SeatBookings = new SelectList(availableSeatBookings, "SeatBookingID", "SeatBookingID");
            return View(ticket);
        }


        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = await _ticketRepository.GetByIdAsync(id.Value);
            if (ticket == null)
            {
                return NotFound();
            }
            ViewBag.SeatBookings = new SelectList(await _seatBookingRepository.GetAllAsync(), "SeatBookingID", "SeatBookingID");
            return View(ticket);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Ticket ticket)
        {
            if (id != ticket.TicketID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                await _ticketRepository.UpdateAsync(ticket);
                return RedirectToAction(nameof(Index));
            }
            ViewBag.SeatBookings = new SelectList(await _seatBookingRepository.GetAllAsync(), "SeatBookingID", "SeatBookingID");
            return View(ticket);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = await _ticketRepository.GetByIdAsync(id.Value);
            if (ticket == null)
            {
                return NotFound();
            }

            return View(ticket);
        }

        [HttpPost, ActionName("DeleteConfirmed")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _ticketRepository.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
