﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WSCinemaNaw.Models
{
    public class Payment
    {
        public int PaymentID { get; set; }
        public int OrderID { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Amount { get; set; }
        public DateTime PaymentDate { get; set; }
        public string PaymentMethod { get; set; }
        public Order? Order { get; set; }
    }
}
