﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WSCinemaNaw.Models
{
    public class Ticket
    {
        public int TicketID { get; set; }
        public int SeatBookingID { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Price { get; set; }

        public SeatBooking? SeatBooking { get; set; }
        public Customer? Customer { get; set; }
        public List<OrderDetail>? OrderDetails { get; set; }
    }
}

