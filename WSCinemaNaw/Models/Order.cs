﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WSCinemaNaw.Models
{
    public class Order
    {
        public int OrderID { get; set; }
        public DateTime OrderDate { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal TotalAmount { get; set; }

        public Customer? Customer { get; set; }
        public List<OrderDetail>? OrderDetails { get; set; }
    }
}
