﻿namespace WSCinemaNaw.Models
{
    public class Room
    {
        public int RoomID { get; set; }
        public string Name { get; set; }
        public int TheaterID { get; set; }
        public List<Seat>? Seats { get; set; }
        public Theater? Theater { get; set; }
        public List<Showtime>? Showtimes { get; set; }
    }
}
