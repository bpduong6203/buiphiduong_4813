﻿namespace WSCinemaNaw.Models
{
    public class SeatBookingViewModel
    {
        public List<int> SeatIDs { get; set; }
        public int ShowtimeID { get; set; }
        public bool IsBooked { get; set; }
        public bool IsAvailable { get; set; }
    }

}
