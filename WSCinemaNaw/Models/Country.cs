﻿namespace WSCinemaNaw.Models
{
    public class Country
    {
        public int CountryID { get; set; }
        public string Name { get; set; }
        public List<Movie>? Movies { get; set; }
    }
}
