﻿namespace WSCinemaNaw.Models
{
    public class MovieDetailsViewModel
    {
        public Movie Movie { get; set; }
        public List<DateTime> Dates { get; set; }
        public List<TimeSpan> Times { get; set; }
        public List<SeatBooking> SeatBookings { get; set; }
    }
}
