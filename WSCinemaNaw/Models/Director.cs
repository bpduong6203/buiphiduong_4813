﻿namespace WSCinemaNaw.Models
{
    public class Director
    {
        public int DirectorID { get; set; }
        public string Name { get; set; }
        public List<Movie>? Movies { get; set; }
    }
}
