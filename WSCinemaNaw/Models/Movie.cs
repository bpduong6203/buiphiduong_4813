﻿namespace WSCinemaNaw.Models
{
    public class Movie
    {
        public int MovieID { get; set; }
        public string Title { get; set; }
        public int DirectorID { get; set; }
        public int CountryID { get; set; }
        public int GenreID { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string? PosterImage { get; set; }
        public string? TrailerVideo { get; set; }
        public List<Showtime>? Showtimes { get; set; }
        public Country? Country { get; set; }
        public Director? Director { get; set; }
        public Genre? Genre { get; set; }
    }
}
