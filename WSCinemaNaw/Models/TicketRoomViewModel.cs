﻿namespace WSCinemaNaw.Models
{
    public class TicketRoomViewModel
    {
        public IEnumerable<Ticket> Tickets { get; set; }
        public IEnumerable<Room> Rooms { get; set; }
    }
}
