﻿namespace WSCinemaNaw.Models
{
    public class Seat
    {
        public int SeatID { get; set; }
        public int RoomID { get; set; }
        public int RowNumber { get; set; }
        public int SeatNumber { get; set; }
        public Room? Room { get; set; }
        public List<SeatBooking>? SeatBookings { get; set; }
    }
}