﻿namespace WSCinemaNaw.Models
{
    public class Theater
    {
        public int TheaterID { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public List<Room>? Rooms { get; set; }
    }
}
