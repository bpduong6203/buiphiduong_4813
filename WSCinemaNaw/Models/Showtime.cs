﻿namespace WSCinemaNaw.Models
{
    public class Showtime
    {
        public int ShowtimeID { get; set; }
        public int MovieID { get; set; }
        public int TheaterID { get; set; }
        public int RoomID { get; set; }
        public DateTime ShowtimeDate { get; set; }
        public Movie? Movie { get; set; }
        public Room? Room { get; set; }
        public Theater? Theater { get; set; }
        public List<SeatBooking>? SeatBookings { get; set; }
    }
}
