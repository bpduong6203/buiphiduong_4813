﻿namespace WSCinemaNaw.Models
{
    public class SeatBooking
    {
        public int SeatBookingID { get; set; }
        public int SeatID { get; set; }
        public int ShowtimeID { get; set; }
        public bool IsBooked { get; set; } = false;
        public bool IsReserved { get; set; } = false; //kiểm tra trạng thái tạm thơi
        public bool IsAvailable { get; set; } = true;//bảo trì 

        public Seat? Seat { get; set; }
        public Showtime? Showtime { get; set; }
        public List<Ticket>? Tickets { get; set; }
    }
}
