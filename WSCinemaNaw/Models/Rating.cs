﻿namespace WSCinemaNaw.Models
{
    public class Rating
    {
        public int RatingID { get; set; }
        public int MovieID { get; set; }
        public int Score { get; set; } // Điểm đánh giá từ 1 đến 5
        public string Comment { get; set; } // Nhận xét của khách hàng

        public Movie? Movie { get; set; }
        public Customer? Customer { get; set; }
    }
}
