﻿using Microsoft.AspNetCore.Identity;

namespace WSCinemaNaw.Models
{
    public class Customer : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<Ticket>? Tickets { get; set; }
    }
}
