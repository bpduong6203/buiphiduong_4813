﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WSCinemaNaw.Models
{
    public class OrderDetail
    {
        public int OrderDetailID { get; set; }
        public int OrderID { get; set; }
        public int TicketID { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Price { get; set; }

        public Order? Order { get; set; }
        public Ticket? Ticket { get; set; }
    }
}
